package com.aj.aggregator.util;

import java.util.concurrent.Executors;

public class AggregatorTest {
  public static void main(String[] args) {

    String response =
        Aggregator.sequentialTask(() -> 0)
            .and(() -> 2.3)
            .join((f, s) -> String.valueOf(f + s))
            .execute()
            .orElse("No Value Found");

    System.out.println("response = " + response);

    String parallelResponse =
        Aggregator.parallelTask(() -> 0)
            .and(() -> 2.3)
            .join((f, s) -> String.valueOf(f + s))
            .withExecutor(null)
            .execute()
            .orElse("No Value Found");

    System.out.println("parallelResponse = " + parallelResponse);

    response =
        Aggregator.sequentialTask(() -> 0)
            .andIfPresent(() -> 2.3)
            .join((f, s) -> String.valueOf(f + s))
            .execute()
            .orElse("No Value Found");

    System.out.println("response = " + response);

    parallelResponse =
        Aggregator.parallelTask(() -> 0)
            .and(() -> 2.3)
            .join((f, s) -> String.valueOf(f + s))
            .withExecutor(Executors.newCachedThreadPool())
            .execute()
            .orElse("No Value Found");

    System.out.println("parallelResponse = " + parallelResponse);
  }
}
