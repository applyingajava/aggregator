package com.aj.aggregator;

import java.util.stream.Stream;

public class AggregateTest {
  public static void main(String[] args) {
    Stream<Integer> integerStream = Stream.of(1, 2);
    Stream<Boolean> booleanStream = integerStream.map(n -> n % 2 == 0);
    long count = booleanStream.count();
    System.out.println("count = " + count);

    Aggregator_V1<Integer> task = Aggregator_V1.AggregateTask_V1.sequentialTask(() -> 1);
    Aggregator_V1.TaskAggregator<Integer, Double> integerDoubleTaskAggregator = task.and(() -> 2.3);
    Aggregator_V1.Zip_V1<Integer, Double, String> integerDoubleStringZip_v1 =
        (f, s) -> String.valueOf(f + s);
    Aggregator_V1.ComposeTaskResponse<Integer, Double, String> combine =
        integerDoubleTaskAggregator.combine(integerDoubleStringZip_v1);
    String execute = combine.execute().get();
    System.out.println("execute = " + execute);

    String execute1 =
        Aggregator_V1.AggregateTask_V1.parallelTask(() -> 1)
            .and(() -> 2.3)
            .combine(integerDoubleStringZip_v1)
            .withExecutor(null)
            .execute()
            .get();
    System.out.println("execute1 = " + execute1);

    Aggregator_V1.ComposeTaskResponse<Integer, Double, String> composeTaskResponse =
        Aggregator_V1.AggregateTask_V1.sequentialTask(() -> 1)
            .and(() -> 2.3)
            .combine(integerDoubleStringZip_v1);

    String orElse =
        Aggregator_V1.AggregateTask_V1.sequentialTask(() -> 1)
            .and(() -> 2.3)
            .combine(integerDoubleStringZip_v1)
            .execute()
            .orElse(composeTaskResponse.execute().get());

    System.out.println("orElse = " + orElse);
  }
}
