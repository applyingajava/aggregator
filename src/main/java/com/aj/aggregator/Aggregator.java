package com.aj.aggregator;

import java.util.function.Supplier;

public abstract class Aggregator<F> {
    protected Supplier<F> task;

    public Aggregator(Supplier<F> task) {
        this.task = task;
    }

    public abstract <S> SequentialAggregator<F, S> andThen(Supplier<S> supplier);

    public static void main(String[] args) {
        Aggregator<Integer> task = AggregateTask.sequentialTask(() -> 1);
        SequentialAggregator<Integer, Double> integerDoubleSequentialAggregator = task.andThen(() -> 2.3);
        ComposeResponse<Integer, Double, String> combine = integerDoubleSequentialAggregator.combine((f, s) -> String.valueOf(f + s));
        String execute = combine.execute();
        System.out.println("execute = " + execute);

    }
}

abstract class SequentialAggregator<F, S> {
    protected Supplier<S> task;
    protected Aggregator<F> parent;

    public SequentialAggregator(Supplier<S> task, Aggregator<F> parent) {
        this.task = task;
        this.parent = parent;
    }

    public abstract <R> ComposeResponse<F, S, R> combine(Zip<F, S, R> zip);
}

abstract class ComposeResponse<F, S, R> {
    protected SequentialAggregator<F, S> sequentialAggregator;

    public ComposeResponse(SequentialAggregator<F, S> sequentialAggregator) {
        this.sequentialAggregator = sequentialAggregator;
    }

    public abstract R execute();

}

class AggregateTask {

    public static <F> Aggregator<F> sequentialTask(Supplier<F> task) {
        return new Aggregator<F>(task) {
            @Override
            public <S> SequentialAggregator<F, S> andThen(Supplier<S> supplier) {
                return new SequentialAggregator<F, S>(supplier, this) {

                    @Override
                    public <R> ComposeResponse<F, S, R> combine(Zip<F, S, R> zip) {
                        return new ComposeResponse<F, S, R>(this) {
                            @Override
                            public R execute() {
                                return zip.process(this.sequentialAggregator.parent.task.get(), this.sequentialAggregator.task.get());
                            }
                        };
                    }
                };
            }
        };
    }
}

interface Zip<F, S, R> {
    R process(F f, S s);
}
