package com.aj.aggregator;

import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.function.Supplier;

public abstract class Aggregator_V1<F> {
    private final Supplier<F> task;

    public Aggregator_V1(Supplier<F> task) {
        this.task = task;
    }

    public abstract <S> TaskAggregator<F, S> and(Supplier<S> supplier);

    static abstract class TaskAggregator<F, S> {
        Supplier<S> task;
        Aggregator_V1<F> parent;

        public abstract <R> ComposeTaskResponse<F, S, R> combine(Zip_V1<F, S, R> zip);
    }

    static class SequentialAggregator_V1<F, S> extends TaskAggregator<F, S> {

        public SequentialAggregator_V1(Supplier<S> task, Aggregator_V1<F> parent) {
            this.task = task;
            this.parent = parent;
        }

        public <R> ComposeTaskResponse<F, S, R> combine(Zip_V1<F, S, R> zip) {
            return new ComposeSeqResponse_V1<>(this, zip);
        }
    }

    static class ParallelAggregator_V1<F, S> extends TaskAggregator<F, S> {

        public ParallelAggregator_V1(Supplier<S> task, Aggregator_V1<F> parent) {
            this.task = task;
            this.parent = parent;
        }

        public <R> ComposeTaskResponse<F, S, R> combine(Zip_V1<F, S, R> zip) {
            return new ComposeParallelResponse_V1<>(this, zip);
        }
    }

    static abstract class ComposeTaskResponse<F, S, R> {
        TaskAggregator<F, S> taskAggregator;
        Zip_V1<F, S, R> zip;

        public abstract Optional<R> execute();

        public abstract ComposeTaskResponse<F, S, R> withExecutor(ExecutorService executor);
    }

    static class ComposeSeqResponse_V1<F, S, R> extends ComposeTaskResponse<F, S, R> {

        public ComposeSeqResponse_V1(TaskAggregator<F, S> sequentialAggregator, Zip_V1<F, S, R> zip) {
            this.taskAggregator = sequentialAggregator;
            this.zip = zip;
        }

        public Optional<R> execute() {
            return Optional.ofNullable(zip.process(this.taskAggregator.parent.task.get(), this.taskAggregator.task.get()));
        }

        @Override
        public ComposeSeqResponse_V1<F, S, R> withExecutor(ExecutorService executor) {
            throw new UnsupportedOperationException();
        }
    }

    static class ComposeParallelResponse_V1<F, S, R> extends ComposeTaskResponse<F, S, R> {
        private ExecutorService executor;

        public ComposeParallelResponse_V1<F, S, R> withExecutor(ExecutorService executor) {
            this.executor = executor;
            return this;
        }

        public ComposeParallelResponse_V1(TaskAggregator<F, S> parallelAggregator, Zip_V1<F, S, R> zip) {
            this.taskAggregator = parallelAggregator;
            this.zip = zip;
        }

        public Optional<R> execute() {
            F firstResponse = this.taskAggregator.parent.task.get();
            if (Objects.isNull(firstResponse)) {
                return Optional.empty();
            }
            S secondResponse = this.taskAggregator.task.get();
            return Optional.ofNullable(zip.process(firstResponse, secondResponse));
        }
    }

    static class AggregateTask_V1 {

        public static <F> Aggregator_V1<F> sequentialTask(Supplier<F> task) {
            return new Aggregator_V1<F>(task) {
                @Override
                public <S> SequentialAggregator_V1<F, S> and(Supplier<S> supplier) {
                    return new SequentialAggregator_V1<>(supplier, this);
                }
            };
        }

        public static <F> Aggregator_V1<F> parallelTask(Supplier<F> task) {
            return new Aggregator_V1<F>(task) {
                @Override
                public <S> ParallelAggregator_V1<F, S> and(Supplier<S> supplier) {
                    return new ParallelAggregator_V1<>(supplier, this);
                }
            };
        }
    }

    public interface Zip_V1<F, S, R> {
        R process(F f, S s);
    }

}

