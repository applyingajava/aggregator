package com.aj.aggregator.util;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.*;
import java.util.function.Supplier;

public abstract class Aggregator<F> {
  private final Supplier<F> task;

  public Aggregator(Supplier<F> task) {
    this.task = task;
  }

  public abstract <S> AbstractAggregator<F, S> and(Supplier<S> supplier);

  public abstract <S> AbstractAggregator<F, S> andIfPresent(Supplier<S> supplier);

  abstract static class AbstractAggregator<F, S> {
    protected Supplier<S> task;
    protected Aggregator<F> parent;

    public abstract <R> ResponseProcessor<F, S, R> join(ZipAccumulator<F, S, R> zipAccumulator);
  }

  static class SequentialAggregator<F, S> extends AbstractAggregator<F, S> {
    private Boolean doIfPresent;

    public SequentialAggregator(Supplier<S> task, Aggregator<F> parent, Boolean doIfPresent) {
      this.task = task;
      this.parent = parent;
      this.doIfPresent = doIfPresent;
    }

    public <R> ResponseProcessor<F, S, R> join(ZipAccumulator<F, S, R> zipAccumulator) {
      return new SequentialResponseProcessor<>(this, zipAccumulator, doIfPresent);
    }
  }

  static class ParallelAggregator<F, S> extends AbstractAggregator<F, S> {

    public ParallelAggregator(Supplier<S> task, Aggregator<F> parent) {
      this.task = task;
      this.parent = parent;
    }

    public <R> ResponseProcessor<F, S, R> join(ZipAccumulator<F, S, R> zipAccumulator) {
      return new ParallelResponseProcessor<>(this, zipAccumulator);
    }
  }

  abstract static class ResponseProcessor<F, S, R> {
    protected AbstractAggregator<F, S> aggregator;
    protected ZipAccumulator<F, S, R> zipAccumulator;

    public abstract Optional<R> execute();

    public abstract ResponseProcessor<F, S, R> withExecutor(ExecutorService executor);
  }

  static class SequentialResponseProcessor<F, S, R> extends ResponseProcessor<F, S, R> {
    private Boolean doIfPresent;

    public SequentialResponseProcessor(
        AbstractAggregator<F, S> sequentialAggregator,
        ZipAccumulator<F, S, R> zipAccumulator,
        Boolean doIfPresent) {
      this.aggregator = sequentialAggregator;
      this.zipAccumulator = zipAccumulator;
      this.doIfPresent = doIfPresent;
    }

    public Optional<R> execute() {
      F firstResponse = this.aggregator.parent.task.get();
      if (Objects.isNull(firstResponse) && doIfPresent) {
        return Optional.empty();
      }
      S secondResponse = this.aggregator.task.get();
      return Optional.ofNullable(zipAccumulator.process(firstResponse, secondResponse));
    }

    @Override
    public SequentialResponseProcessor<F, S, R> withExecutor(ExecutorService executor) {
      throw new UnsupportedOperationException();
    }
  }

  static class ParallelResponseProcessor<F, S, R> extends ResponseProcessor<F, S, R> {
    private ExecutorService executor;

    public ParallelResponseProcessor<F, S, R> withExecutor(ExecutorService executor) {
      this.executor = executor;
      return this;
    }

    public ParallelResponseProcessor(
        AbstractAggregator<F, S> parallelAggregator, ZipAccumulator<F, S, R> zipAccumulator) {
      this.aggregator = parallelAggregator;
      this.zipAccumulator = zipAccumulator;
    }

    public Optional<R> execute() {
      if (Objects.nonNull(executor)) {
        Callable<Object> callableFirst = () -> this.aggregator.parent.task.get();
        Callable<Object> callableSecond = () -> this.aggregator.task.get();
        try {
          List<Future<Object>> futureList =
              executor.invokeAll(Arrays.asList(callableFirst, callableSecond), 1, TimeUnit.MINUTES);
          F firstResponse = (F) (futureList.get(0).isDone() ? futureList.get(0).get() : 0);
          S secondResponse = (S) (futureList.get(1).isDone() ? futureList.get(1).get() : 0);
          return Optional.ofNullable(zipAccumulator.process(firstResponse, secondResponse));
        } catch (InterruptedException e) {
          throw new RuntimeException();
        } catch (ExecutionException e) {
          e.printStackTrace();
        } finally {
          executor.shutdown();
        }

      } else {
        SequentialResponseProcessor<F, S, R> sequentialResponseProcessor =
            new SequentialResponseProcessor<>(aggregator, zipAccumulator, false);
        Optional<R> result = sequentialResponseProcessor.execute();
        return result;
      }
      return null;
    }
  }

  public static <F> Aggregator<F> sequentialTask(Supplier<F> task) {
    return new Aggregator<F>(task) {
      @Override
      public <S> Aggregator.SequentialAggregator<F, S> and(Supplier<S> supplier) {
        return new Aggregator.SequentialAggregator<>(supplier, this, false);
      }

      @Override
      public <S> AbstractAggregator<F, S> andIfPresent(Supplier<S> supplier) {
        return new Aggregator.SequentialAggregator<>(supplier, this, true);
      }
    };
  }

  public static <F> Aggregator<F> parallelTask(Supplier<F> task) {
    return new Aggregator<F>(task) {
      @Override
      public <S> Aggregator.ParallelAggregator<F, S> and(Supplier<S> supplier) {
        return new Aggregator.ParallelAggregator<>(supplier, this);
      }

      @Override
      public <S> AbstractAggregator<F, S> andIfPresent(Supplier<S> supplier) {
        throw new UnsupportedOperationException();
      }
    };
  }
}
