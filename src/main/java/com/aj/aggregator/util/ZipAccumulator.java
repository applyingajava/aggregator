package com.aj.aggregator.util;

public interface ZipAccumulator<F, S, R> {
  R process(F f, S s);
}
